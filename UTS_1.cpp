#include <iostream>
using namespace std;

int main()
{
  string Nama = "Ardi Kamal Karima";
  string Kelas = "1C";
  int Nim = 301230023;

  cout << "-------------------KALKULATOR------------------" << endl;
  cout << "Nama Pembuat : " << Nama << endl;
  cout << "Kelas        : " << Kelas << endl;
  cout << "NIM          : " << Nim << endl;

  double bil1, bil2;
  char op;

  cout << endl << "PROGRAM :" << endl;

  cout << "Masukan Bilangan ke 1      : ";
  cin >> bil1;

  cout << "Masukan Operator (x, /, +, -) : ";
  cin >> op;

  cout << "Masukan Bilangan ke 2      : ";
  cin >> bil2;

  if (op == '+') 
  {
    cout << "Bilangan ke 1 + Bilangan ke 2 = " << bil1 << " + " << bil2 << " = " << bil1 + bil2 << endl;
  }
  else if (op == 'x') 
  {
    cout << "Bilangan ke 1 x Bilangan ke 2 = " << bil1 << " x " << bil2 << " = " << bil1 * bil2 << endl;
  }
  else if (op == '/') // change 'x' to ':'
  {
    if (bil2 != 0)  // check for division by zero
    {
      cout << "Bilangan ke 1 / Bilangan ke 2 = " << bil1 << " / " << bil2 << " = " << bil1 / bil2 << endl;
    }
    else
    {
      cout << "Error: Division by zero is undefined." << endl;
    }
  }
  else if (op == '-') // add subtraction operation
  {
    cout << "Bilangan ke 1 - Bilangan ke 2 = " << bil1 << " - " << bil2 << " = " << bil1 - bil2 << endl;
  }
  else
  {
    cout << "Operator tidak valid!" << endl;
  }

  cout << "" << endl;
  cout << "----------------SEMOGA MEMBANTU----------------" << endl;

  return 0;
}
